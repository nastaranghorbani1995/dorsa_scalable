from django.urls import path

from .views import sum_view, history_view

urlpatterns = [
    path('sum/', sum_view),
    path('history/', history_view)
]