from rest_framework.decorators import api_view, throttle_classes, permission_classes
from rest_framework.throttling import UserRateThrottle
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status

from .models import Numbers
from .serializers import NumbersSerializer


@throttle_classes([UserRateThrottle])
@api_view(["GET"])
@permission_classes([AllowAny])
def sum_view(request):
    try:
        data = {
            'number_a': request.GET.get("a"),
            'number_b': request.GET.get("b")
        }
        serializer = NumbersSerializer(data=data)

        if not serializer.is_valid():
            return Response(
                {
                    "status": False,
                    "message": serializer.errors,
                    "data": None

                },
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response(
            {
                "message": "success",
                "result": serializer.data["sum"],
                "status": True
            },
            status=status.HTTP_200_OK
        )
    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def history_view(request):
    try:
        all_numbers = Numbers.objects.all().order_by("-id")
        serializer = NumbersSerializer(instance=all_numbers, many=True)

        return Response(
            {
                "message": "success",
                "result": serializer.data,
                "status": True
            },
            status=status.HTTP_200_OK
        )
    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )