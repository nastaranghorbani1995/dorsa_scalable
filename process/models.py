from django.db import models


class Numbers(models.Model):
    number_a = models.IntegerField()
    number_b = models.IntegerField()

    @property
    def sum(self):
        "Returns the sum af two number"
        return (self.number_a) + (self.number_b)

    def __str__(self):
        return f'{self.number_a} , {self.number_b}'


class TotalSaver(models.Model):
    '''
    the purpose of this table is that if the #request increase
    we have the latest total amount without missing
    '''
    total_number = models.BigIntegerField()

    def __str__(self):
        return self.total_number

