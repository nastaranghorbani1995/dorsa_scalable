from rest_framework import serializers

from process.models import Numbers


class NumbersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Numbers
        fields = [
            "number_a",
            "number_b",
            "sum"
        ]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['a'] = instance.number_a
        data['b'] = instance.number_b
        data.pop('number_a')
        data.pop('number_b')
        data.pop('sum')
        return data