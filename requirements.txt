# django
Django == 2.2.4
django-cors-headers == 3.0.2
django-simple-history == 2.7.3
djangorestframework == 3.10.2
jsonfield == 3.1.0
djangorestframework-simplejwt==4.5.0
django-currentuser==0.5.3
django-celery-beat==2.4.0



# db
mysqlclient == 1.4.2  # .post1
PyMySQL == 0.9.3

# web & net
requests


# authentication & crypto
rsa == 4.0
crypto == 1.4.1
pycrypto == 2.6.1
oauth2client == 4.1.3
pyDes == 2.0.1
pycryptodome==3.15.0

# other
uwsgi
pyjwt==1.7.1

