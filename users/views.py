from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from django.utils.translation import ugettext as _
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import get_user_model

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from .models import CustomUser


class LogoutView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        try:
            username = request.data.get('username')
            password = request.data.get('password')
            if not username or not password:
                return Response({"status": False, "data": None, "message": _("empty data")},
                                status=status.HTTP_400_BAD_REQUEST)

            user = authenticate(username=username, password=password)

            if user is not None:

                try:
                    user_query = CustomUser.objects.filter(user_id=user.id).get()

                except Exception as e:
                    return Response({"status": False, "data": None, "message": _("user not found")}, status=status.HTTP_404_NOT_FOUND)

                user.id = str(user_query.id)

                refresh = TokenObtainPairSerializer.get_token(user)
                # refresh = RefreshToken.for_user(user_obj)

                return JsonResponse({'status': True,
                                     "data":
                                         {
                                             'access': str(refresh.access_token),
                                             'refresh': str(refresh),
                                             'user': user_query.id
                                         },

                                     "message": _("Success")
                                     })

            else:
                return JsonResponse({"status": False, "data": None, "message": _("forbidden")},
                                    status=status.HTTP_403_FORBIDDEN)

        except Exception as e:
            return Response({"status": False, "data": None, "message": _("forbidden")},
                                    status=status.HTTP_404_NOT_FOUND)

